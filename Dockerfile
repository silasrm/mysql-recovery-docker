FROM mysql:5.7

EXPOSE 3306
CMD ["/usr/bin/mysqld_safe", "--skip-grant-tables", "--skip-networking"]
