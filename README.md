# MySQL Recovery Docker #

Usando o docker para recuperar **backup** de mysql. 


## Observações ##

* O **backup** neste caso é a pasta onde fica os dados do MySQL, a __datadir__. No Linux geralmente fica em **/var/lib/mysql/**
* Crie um link simbólico da __datadir__ desejada para __./mysql__ ou copie os arquivos para a raíz da pasta pai da __datadir__.


## Passos ##

* Permissão na pasta __./sqlfiles__: **chmod -R 07777 ./sqlfiles**
* Execute: **docker-compose up -d**
* Execute: **docker exec -it mysql_databases bash -c "/tmp/sqlfiles/export.sh"**


Se tudo estiver executado corretamente, na pasta __./sqlfiles__ vai ter um arquivo para cada base de dados.


## Erro ##

Se ocorrer o erro 

	mysqldump: Couldn't execute 'SHOW VARIABLES LIKE 'gtid\_mode'': Table 'performance_schema.session_variables' doesn't exist (1146)
	
### Passos ###

* Execute, para entrar no container: **docker exec -i -t mysql_databases /bin/bash**
* Dentro dele, execute: **mysql_upgrade -u root --force**
* Espere finalizar e reinicie o docker e execute o comando do backup.


Baseado em: (http://geekhub.vn/recovery-mysql-data-using-docker/)http://geekhub.vn/recovery-mysql-data-using-docker/